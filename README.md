# NestJS Boilerplate

A delightful way to building a [NestJS](https://nestjs.com/) RESTful API using PostgreSQL, TypeORM, Docker.

`Note: This template is under development.`

---
## Features

- Using [PostgreSQL](https://www.postgresql.org/) for database
- Simplified Database Query with the ORM [TypeORM](https://typeorm.io/#/). Thanks to TypeORM, we can switch easily with other db as MySQL, SQLite ...
- Authentication with [Passport](https://github.com/jaredhanson/passport), [JWT](https://github.com/mikenicholson/passport-jwt), Refresh token (handle with cookie)
- Integration with API documents with [Swagger](https://swagger.io/)
- Testing: Unit testing (mock), End to end testing with [Jest](https://github.com/facebook/jest) & [Supertest](https://github.com/visionmedia/supertest)
- Validation thanks to [class-validator](https://github.com/typestack/class-validator)
- Easy handle HTTP exceptions with NestJS
- More secure app with [helmet](https://helmetjs.github.io/)
- Format code with Eslint & Prettier
- Using option with Docker
- CI-CD: with [Github action](https://github.com/features/actions), [CircleCI](https://circleci.com/) & [TravisCI](https://www.travis-ci.com/)

## Getting Started

This project intended to be used with the latest active version LTS of Nodejs & NestJS.

- To use this project, you can clone this repository by following commands:

  ```bash
  $ git clone https://github.com/tienduy-nguyen/nest-boilerplate-typeorm
  $ cd node-typescript-boilerplate
  $ yarn

  ```
- Remove `example` from file name of `.env.docker.example` and `.env.example` and replace your desire secret variables.

  Example of `.env.docker` file (for postgres & pdadmin image)
  ```.env
  POSTGRES_USER=postgres
  POSTGRES_PASSWORD=postgres
  POSTGRES_DB=test_db
  PGADMIN_DEFAULT_EMAIL=admin@admin.com
  PGADMIN_DEFAULT_PASSWORD=admin
  ELASTIC_PASSWORD=admin

  ```

  Example of `.env` file:

  ```.env
  SERVER_PORT=1776
  ROUTE_GLOBAL_PREFIX=/api
  JWT_SECRET=justanotherworld
  JWT_EXPIRATION_TIME=2h
  JWT_REFRESH_TOKEN_SECRET=someothersecret
  JWT_REFRESH_TOKEN_EXPIRATION_TIME=30d

  # Typeorm
  TYPEORM_CONNECTION = postgres
  TYPEORM_HOST = postgres
  TYPEORM_USERNAME = postgres
  TYPEORM_PASSWORD = postgres
  TYPEORM_DATABASE = test_db
  TYPEORM_PORT = 5432
  TYPEORM_ENTITIES = [src/modules/**/*.entity.ts]

  # For run migration cli
  TYPEORM_MIGRATIONS=[src/common/migrations/**/*.ts]
  TYPEORM_MIGRATIONS_DIR=src/common/migrations

  # Redis
  # Host: 'redis' from docker-compose
  REDIS_HOST=redis
  REDIS_PORT=6379

  ```
- Run server locally
  ```bash
  $ yarn start:dev
  ```

  By default, when you run server successfully, you can go to `http://localhost:1776/api` to check server.

  Check api docs with **swagger** at route: `http://localhost:1776/api/docs`

## Using Docker

Make sure you have Docker installed on you machine.

For this template, I use docker to run server with `postgres` and `pdadmin4`.

To run docker in the development, we can just simple use:
- `docker-compose build`: build and run all services in `docker-compose.yml` file
- `docker-compose up`: run all services
- `docker-compose down`: stop all services
- `docker-compose up <service-name>` to run only one service. (Thinking to use with option `-d` detached)

I recommend using [portainer](https://www.portainer.io/) to manage multi services of docker.

**Note**: In case you don't want to use docker, you need to have `postgres` installed in your machine. And you can consider install `pgadmin` to GUI management database postgres too.

Then you have to modify variables of TypeORM in `.env` file that match with your local postgres account.
